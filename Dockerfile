FROM quay.io/mlvtito/java-8-zulu:latest

ENV GLASSFISH_ARCHIVE payara5
ENV INSTALL_DIR /opt

RUN wget -O /tmp/${GLASSFISH_ARCHIVE}.zip -L https://s3-eu-west-1.amazonaws.com/payara.fish/Payara+Downloads/5.181/payara-5.181.zip \ 
    && unzip /tmp/${GLASSFISH_ARCHIVE}.zip -d ${INSTALL_DIR} \ 
    && rm /tmp/${GLASSFISH_ARCHIVE}.zip \
    && ln -s ${INSTALL_DIR}/${GLASSFISH_ARCHIVE}/glassfish /opt/glassfish

ENV GLASSFISH_HOME /opt/glassfish
ENV DEPLOYMENT_DIR ${GLASSFISH_HOME}/domains/domain1/autodeploy
ENV PATH "$PATH":/${GLASSFISH_HOME}/bin

WORKDIR ${GLASSFISH_HOME}/bin
ENTRYPOINT asadmin start-domain --verbose domain1
EXPOSE 4848 8009 8080 8181

